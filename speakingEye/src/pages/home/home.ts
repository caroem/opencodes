import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions, CameraPreviewDimensions } from '@ionic-native/camera-preview';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { FileTransfer, FileUploadOptions, FileUploadResult, FileTransferObject, FileTransferError } from '@ionic-native/file-transfer';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  var: string;
  image: string;
  options: CameraPreviewPictureOptions;
  cameraPreviewOpts: CameraPreviewOptions;
  isListening: Boolean;
  speechMatches: String[];
  productLUT: Object;

  constructor(public navCtrl: NavController, private platform: Platform, private camera: CameraPreview, private tts: TextToSpeech, private stt: SpeechRecognition, private transfer: FileTransfer) {
    this.productLUT={
      'kidney_ja': { name: 'Ja! Kidney Beans', phonetic: 'Yaah Kidney Beans'},
      'kidney_rewe': { name: 'Rewe Kidney Beans', phonetic: 'Raveh Kidney Beans'},
      'mais_ja': { name: 'Ja! Corn', phonetic: 'Yah corn'},
      'mais_rewe': { name: 'Bonduelle Corn', phonetic: 'Bonduelle corn'},
      'kokosmilch': { name: 'Coconut Milk', phonetic: 'Coconut milk'}
    };
    this.isListening=false;
    this.speechMatches=[];
    this.cameraPreviewOpts = {
      x: 0,
      y: 0,
      width: 1,
      height: 1,
      camera: 'rear',
      tapPhoto: false,
      previewDrag: false,
      toBack: true,
      alpha: 0 
    };
    
    this.options = {
      quality: 90,
      // destinationType: this.camera.DestinationType.DATA_URL,
      // encodingType: this.camera.EncodingType.JPEG,
      // mediaType: this.camera.MediaType.PICTURE,
      // saveToPhotoAlbum: false,
      // allowEdit: false,
      // correctOrientation: true
    };

    // platform.ready().then((()=>{
    //   this.var='Initializing...';
    //   this.isListening=false;
    //   this.stt.hasPermission().then(hasPermission=>{
    //     if(hasPermission) {
    //       this.var='Ready.';
    //       // this.listen();
    //     }else {          
    //      this.stt.requestPermission().then((hasPermission=>{
    //       if(hasPermission) {
    //         this.var='Ready.';
    //       }
    //      }).bind(this)); 
    //     }
    //   });
    // }).bind(this));
    platform.ready().then(this.preparePhoto.bind(this));

    this.var="";
    this.image="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=";
  }

  listen() {
    var me=this;
    if(this.isListening) {
      this.stt.stopListening();
      this.isListening=false;
    }else {
      this.isListening=true;
      this.tts.speak({'rate': 1.6, 'text': 'I am listening.'}).then((() => {
        this.stt.startListening({'language': 'en-US'}).subscribe(((matches: Array<String>)=>{
          me.var=matches.join(' : ');
          me.speechMatches.concat(matches);
        }).bind(this));
      }).bind(this));
    }
  }

  preparePhoto() {
    this.var="Preparing..."
    let promises: Promise<any>[]=[this.tts.speak({rate: 1.75, text: 'Initializing.'})];
    promises.push(this.camera.startCamera(this.cameraPreviewOpts));
    Promise.all(promises).then((()=>{
        this.var="Ready"
        this.tts.speak({rate: 1.65, text: 'Tap the screen to take a picture.'});
      }).bind(this)
     ).catch((() => {
       this.var='Error initializing camera.';
       this.tts.speak({rate: 1.65, text: 'I am blind.'});
      }).bind(this)
    );
  }

  takePhoto() {
    this.var="Capturing image...";
    let promises: Promise<any>[]=[this.tts.speak({rate: 1.75, text: 'Capturing.'})];
    promises.push(this.camera.takePicture(this.options));
    Promise.all(promises).then(((data) => {
      let imageData: String=data[1];
      this.var="Analyzing image...";
      this.image='data:image/jpeg;base64,'+imageData;
      promises=[this.tts.speak({rate: 1.75, text: 'Analyzing.'})];
      let transferOpts: FileUploadOptions={
        'fileKey': 'file',
        'fileName': 'image.jpg',
        'httpMethod': 'POST',
        'mimeType': 'image/jpeg',
        'chunkedMode': false,
        headers: {
          'Prediction-Key': 'd3c0118f34a0484b83dae3fad50cbd05'
        }
      };
      let upload=this.transfer.create();
      promises.push(upload.upload(this.image,'https://southcentralus.api.cognitive.microsoft.com/customvision/v1.1/Prediction/3af68434-e1d6-4139-9f9a-d669bc54ca46/image?iterationId=1c502089-e2d7-43cc-bd22-07dbfd330671',
        transferOpts, true
      ));
      Promise.all(promises).then(((data) => {
        let resData: FileUploadResult=data[1];
        let results=JSON.parse(resData.response).Predictions;
        results.sort((a, b) => {
          return b.Probability-a.Probability;
        });
        let highest=results.shift();
        let second=results.shift();
        let status='';
        let text='';
        if(highest.Probability<0.09) {
          status=this.productLUT[highest.Tag].name+': '+highest.Probability;
          text='I see nothing.';
        }else if(highest.Probability/second.Probability<2) {
          status=this.productLUT[highest.Tag].name+': '+highest.Probability+' / '+this.productLUT[second.Tag].name+': '+second.Probability;
          text='Not sure what I see. Maybe its '+this.productLUT[highest.Tag].phonetic;
        }else {
          status=this.productLUT[highest.Tag].name+': '+highest.Probability;
          text=highest.Probability<0.20?'I think its ':'Its';
          text+=this.productLUT[highest.Tag].phonetic;
        }
        this.var=status;
        this.tts.speak({'rate': 1.65, text: text});
      }).bind(this)).catch(((err) => {
        let transferErr: FileTransferError=err;
        this.var=transferErr.body;
      }).bind(this));
    }).bind(this)).catch((e=> {
      this.var="Fucked";
    }).bind(this));
  }
}
